/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ["pocketbase.hive.thebeijinho.com"],
  },
};

export default nextConfig;
