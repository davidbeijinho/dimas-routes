/**
 * This file was @generated using pocketbase-typegen
 */

import type PocketBase from "pocketbase";
import type { RecordService } from "pocketbase";

export enum Collections {
  Places = "places",
  Routes = "routes",
  Users = "users",
}

// Alias types for improved usability
export type IsoDateString = string;
export type RecordIdString = string;
export type HTMLString = string;

// System fields
export type BaseSystemFields<T = never> = {
  id: RecordIdString;
  created: IsoDateString;
  updated: IsoDateString;
  collectionId: string;
  collectionName: Collections;
  expand?: T;
};

export type AuthSystemFields<T = never> = {
  email: string;
  emailVisibility: boolean;
  username: string;
  verified: boolean;
} & BaseSystemFields<T>;

// Record types for each collection

export type PlacesRecord = {
  coordinates?: string;
  description?: HTMLString;
  image?: string;
  name: string;
};

export type RoutesRecord = {
  description?: HTMLString;
  name?: string;
  places: RecordIdString[];
};

export type UsersRecord = {
  avatar?: string;
  name?: string;
};

// Response types include system fields and match responses from the PocketBase API
export type PlacesResponse<Texpand = unknown> = Required<PlacesRecord> &
  BaseSystemFields<Texpand>;
export type RoutesResponse<Texpand = unknown> = Required<RoutesRecord> &
  BaseSystemFields<Texpand>;
export type UsersResponse<Texpand = unknown> = Required<UsersRecord> &
  AuthSystemFields<Texpand>;

// Types containing all Records and Responses, useful for creating typing helper functions

export type CollectionRecords = {
  places: PlacesRecord;
  routes: RoutesRecord;
  users: UsersRecord;
};

export type CollectionResponses = {
  places: PlacesResponse;
  routes: RoutesResponse;
  users: UsersResponse;
};

// Type for usage with type asserted PocketBase instance
// https://github.com/pocketbase/js-sdk#specify-typescript-definitions

export type TypedPocketBase = PocketBase & {
  collection(idOrName: "places"): RecordService<PlacesResponse>;
  collection(idOrName: "routes"): RecordService<RoutesResponse>;
  collection(idOrName: "users"): RecordService<UsersResponse>;
};
