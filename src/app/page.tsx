"use client";
import RoutesList from "../components/RoutesList";
import { getRoutes } from "@/lib/pocketbase";
import React, { useEffect, useState } from "react";
export default function Page() {
  const [routes, setRoutes] = useState<any>({});

  useEffect(() => {
    async function fetchData() {
      const response = await getRoutes();
      setRoutes(response);
    }

    fetchData();
  }, []);
  return (
    <div>
      <h3>Route list:</h3>
      {routes?.items?.length ? (
        <RoutesList routes={routes.items} />
      ) : (
        <p>Loading</p>
      )}
    </div>
  );
}
