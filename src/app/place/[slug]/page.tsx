"use client";
import RoutesList from "@/components/RoutesList";
import { getPlace } from "@/lib/pocketbase";
import React, { useEffect, useState } from "react";
import Link from "next/link";
export default function Page({ params }: { params: { slug: string } }) {
  const [place, setPlace] = useState<any>({});
  useEffect(() => {
    async function fetchPost() {
      try {
        const response = await getPlace({ id: params.slug });
        if (response) {
          setPlace(response);
        }
      } catch (error) {
        console.log("error", error);
      }
    }

    fetchPost();
  }, [params.slug]);

  return (
    <div>
      {place?.expand ? (
        <>
          <Link className="text-blue-600" href="/">
            <h4>Home</h4>
          </Link>
          <hr />
          <h1>Place {place.name}</h1>
          <h2>description:</h2>
          <div dangerouslySetInnerHTML={{ __html: place.description }} />
          <hr />
          <h2>In routes:</h2>
          <>
            <h3>Route:</h3>
            <RoutesList routes={place?.expand?.routes_via_places} />
          </>
        </>
      ) : (
        ""
      )}
    </div>
  );
}
