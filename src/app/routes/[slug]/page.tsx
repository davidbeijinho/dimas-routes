"use client";

import { getRoute } from "@/lib/pocketbase";
import React, { useEffect, useState, useMemo } from "react";
import Link from "next/link";

import { useMap } from "react-leaflet";
import Drawer from "@/components/Drawer";
import PlacesList from "@/components/PlacesList";
import RouteMap from "@/components/RouteMap";

export default function Page({ params }: { params: { slug: string } }) {
  const [route, setRoute] = useState<any>({});
  const [center, setCenter] = useState<any>([]);
  useEffect(() => {
    async function fetchPost() {
      try {
        const response = await getRoute({ id: params.slug });
        if (response) {
          setRoute(response);
          setCenter(response?.expand?.places[0].coordinates.split(","));
        }
      } catch (error) {
        console.log("error", error);
      }
    }

    fetchPost();
  }, [params.slug]);

  const polyline = useMemo(
    () =>
      route?.expand?.places?.map((point: any) => point?.coordinates.split(",")),
    [route],
  );
  return (
    <>
      {center?.length && polyline?.length ? (
        <div>
          <Link className="text-blue-600" href="/">
            <h4>Home</h4>
          </Link>
          <hr />
          <h1>Route: {route.name}</h1>
          <RouteMap route={route} center={center} line={polyline} />
          <h2>description:</h2>
          <div dangerouslySetInnerHTML={{ __html: route.description }} />
          <hr />
          <h2>route places:</h2>
          <PlacesList places={route.expand?.places} />
          {/* <Drawer >
            <PlacesList places={route.expand?.places} onUpdate={handler} />
          </Drawer> */}
        </div>
      ) : (
        "Loading"
      )}
    </>
  );
}
