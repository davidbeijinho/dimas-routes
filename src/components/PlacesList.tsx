import type { PlacesResponse } from "@/../pocketbase-types";

export default function PlacesList({
  places,
  // onUpdate,
}: {
  // onUpdate: any;
  places: PlacesResponse[];
}) {
  // const handleClick = (coordinates: string) => () => {
  //   console.log("heyy", coordinates.split(","));
  //   onUpdate(coordinates.split(","));
  // };
  return (
    <ol className="list-decimal ml-5">
      {places.map((route) => (
        <li key={route.id}>
          {/* <button onClick={handleClick(route.coordinates)}>{route.name}</button> */}
          {route.name}
        </li>
      ))}
    </ol>
  );
}
