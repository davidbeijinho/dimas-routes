import Map from "@/components/Map";
// import Map from "@/components/Map/DynamicMap";

import { forwardRef } from "react";
import Link from "next/link";
import { getImageUrl } from "@/lib/pocketbase";
import Image from "next/image";
import { PlacesResponse, RoutesResponse } from "@/../pocketbase-types";
import { Popup } from "react-leaflet/Popup";
import { Marker } from "react-leaflet/Marker";
import { Polyline } from "react-leaflet/Polyline";
import { TileLayer } from "react-leaflet/TileLayer";
import type { LatLngTuple } from "leaflet";
import RoutePolyline from "./RoutePolyline";
interface PlaceProps extends RoutesResponse {
  expand: {
    places: PlacesResponse[];
  };
}

export default function RouteMap({
  route,
  center,
  line,
}: {
  center: [string, string];
  route: PlaceProps;
  line: LatLngTuple[];
}) {
  //   var Jawg_Dark = L.tileLayer('https://tile.jawg.io/jawg-dark/{z}/{x}/{y}{r}.png?access-token={accessToken}', {
  // 	attribution: '<a href="https://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  // 	minZoom: 0,
  // 	maxZoom: 22,
  // 	accessToken: '<your accessToken>'
  // });

  //   var Stadia_StamenTonerBackground = L.tileLayer('https://tiles.stadiamaps.com/tiles/stamen_toner_background/{z}/{x}/{y}{r}.{ext}', {
  // 	minZoom: 0,
  // 	maxZoom: 20,
  // 	attribution: '&copy; <a href="https://www.stadiamaps.com/" target="_blank">Stadia Maps</a> &copy; <a href="https://www.stamen.com/" target="_blank">Stamen Design</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  // 	ext: 'png'
  // });

  // var Esri_WorldGrayCanvas = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
  // 	attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
  // 	maxZoom: 16
  // });

  // var TopPlusOpen_Grey = L.tileLayer('http://sgx.geodatenzentrum.de/wmts_topplus_open/tile/1.0.0/web_grau/default/WEBMERCATOR/{z}/{y}/{x}.png', {
  // 	maxZoom: 18,
  // 	attribution: 'Map data: &copy; <a href="http://www.govdata.de/dl-de/by-2-0">dl-de/by-2-0</a>'
  // });

  // var Stadia_StamenToner = L.tileLayer('https://tiles.stadiamaps.com/tiles/stamen_toner/{z}/{x}/{y}{r}.{ext}', {
  //   minZoom: 0,
  //   maxZoom: 20,
  //   attribution: '&copy; <a href="https://www.stadiamaps.com/" target="_blank">Stadia Maps</a> &copy; <a href="https://www.stamen.com/" target="_blank">Stamen Design</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  //   ext: 'png'
  // });

  return (
    <Map width="800" height="400" center={center} zoom={15} className="z-10">
      <>
        <TileLayer
          // url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          url="http://sgx.geodatenzentrum.de/wmts_topplus_open/tile/1.0.0/web_grau/default/WEBMERCATOR/{z}/{y}/{x}.png"
          attribution="Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ"
          // attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        {route?.expand?.places.map((point: any) => (
          <Marker position={point.coordinates.split(",")} key={point.id}>
            <Popup>
              <Link href={`/place/${point.id}`}>
                {point.name}
                <Image
                  src={getImageUrl({
                    collection: "Places",
                    record: point.id,
                    filename: point.image,
                  })}
                  width={384}
                  height={384}
                  className="w-64  max-w-64"
                  alt="point image"
                />
              </Link>
            </Popup>
          </Marker>
        ))}
        <RoutePolyline line={line} />
        {/* <Polyline pathOptions={{ color: "black" }} positions={line} /> */}
      </>
    </Map>
  );
}
