import {
  TypedPocketBase,
  Collections,
  RoutesResponse,
  PlacesResponse,
} from "@/../pocketbase-types";
import PocketBase from "pocketbase";

const URL = "https://pocketbase.hive.thebeijinho.com";
const pb = new PocketBase(URL) as TypedPocketBase;

export const getRoutes = async () => {
  const result = await pb.collection(Collections.Routes).getList(1, 50, {});

  return result;
};
type Texpand = {
  places: PlacesResponse[];
};

export const getRoute = async ({ id }: { id: string }) => {
  const result = await pb
    .collection(Collections.Routes)
    .getOne<RoutesResponse<Texpand>>(id, {
      expand: "places",
    });

  return result;
};

export const getPlace = async ({ id }: { id: string }) => {
  const result = await pb.collection(Collections.Places).getOne(id, {
    expand: "routes_via_places",
  });

  return result;
};

export const getImageUrl = ({
  collection,
  record,
  filename,
}: {
  collection: string;
  record: string;
  filename: string;
}) => {
  //http://127.0.0.1:8090/api/files/COLLECTION_ID_OR_NAME/RECORD_ID/FILENAME

  // const url = pb.files.getUrl(record, firstFilename, {'thumb': '100x250'});
  // return url
  // ?token=
  return `${URL}/api/files/${collection}/${record}/${filename}`;
};
